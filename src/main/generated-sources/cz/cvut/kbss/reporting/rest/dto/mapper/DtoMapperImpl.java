package cz.cvut.kbss.reporting.rest.dto.mapper;

import cz.cvut.kbss.reporting.dto.OccurrenceReportDto;
import cz.cvut.kbss.reporting.model.CorrectiveMeasureRequest;
import cz.cvut.kbss.reporting.model.OccurrenceReport;
import java.util.HashSet;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2016-11-08T21:43:12+0100",
    comments = "version: 1.0.0.Final, compiler: javac, environment: Java 1.8.0_74 (Oracle Corporation)"
)
@Component
public class DtoMapperImpl extends DtoMapper {

    @Override
    public OccurrenceReportDto occurrenceReportToOccurrenceReportDto(OccurrenceReport report) {
        if ( report == null ) {
            return null;
        }

        OccurrenceReportDto occurrenceReportDto = new OccurrenceReportDto();

        occurrenceReportDto.setOccurrence( report.getOccurrence() );
        occurrenceReportDto.setFactorGraph( occurrenceToFactorGraph( report.getOccurrence() ) );
        occurrenceReportDto.setId( report.getId() );
        occurrenceReportDto.setFileNumber( report.getFileNumber() );
        occurrenceReportDto.setPhase( report.getPhase() );
        occurrenceReportDto.setAuthor( report.getAuthor() );
        occurrenceReportDto.setDateCreated( report.getDateCreated() );
        occurrenceReportDto.setLastModified( report.getLastModified() );
        occurrenceReportDto.setLastModifiedBy( report.getLastModifiedBy() );
        occurrenceReportDto.setRevision( report.getRevision() );
        occurrenceReportDto.setSeverityAssessment( report.getSeverityAssessment() );
        if ( report.getCorrectiveMeasures() != null ) {
            occurrenceReportDto.setCorrectiveMeasures( new HashSet<CorrectiveMeasureRequest>( report.getCorrectiveMeasures() ) );
        }
        occurrenceReportDto.setSummary( report.getSummary() );

        return occurrenceReportDto;
    }

    @Override
    public OccurrenceReport occurrenceReportDtoToOccurrenceReport(OccurrenceReportDto dto) {
        if ( dto == null ) {
            return null;
        }

        OccurrenceReport occurrenceReport = new OccurrenceReport();

        occurrenceReport.setOccurrence( factorGraphToOccurrence( dto.getFactorGraph() ) );
        occurrenceReport.setId( dto.getId() );
        occurrenceReport.setFileNumber( dto.getFileNumber() );
        occurrenceReport.setPhase( dto.getPhase() );
        occurrenceReport.setAuthor( dto.getAuthor() );
        occurrenceReport.setDateCreated( dto.getDateCreated() );
        occurrenceReport.setLastModified( dto.getLastModified() );
        occurrenceReport.setLastModifiedBy( dto.getLastModifiedBy() );
        occurrenceReport.setRevision( dto.getRevision() );
        occurrenceReport.setSeverityAssessment( dto.getSeverityAssessment() );
        if ( dto.getCorrectiveMeasures() != null ) {
            occurrenceReport.setCorrectiveMeasures( new HashSet<CorrectiveMeasureRequest>( dto.getCorrectiveMeasures() ) );
        }
        occurrenceReport.setSummary( dto.getSummary() );

        return occurrenceReport;
    }
}
