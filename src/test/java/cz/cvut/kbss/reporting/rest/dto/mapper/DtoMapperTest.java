/**
 * Copyright (C) 2016 Czech Technical University in Prague
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.kbss.reporting.rest.dto.mapper;

import cz.cvut.kbss.reporting.config.RestConfig;
import cz.cvut.kbss.reporting.dto.OccurrenceReportDto;
import cz.cvut.kbss.reporting.dto.event.FactorGraphEdge;
import cz.cvut.kbss.reporting.environment.config.MockServiceConfig;
import cz.cvut.kbss.reporting.model.Occurrence;
import cz.cvut.kbss.reporting.model.OccurrenceReport;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {RestConfig.class, MockServiceConfig.class})
public class DtoMapperTest {

    @Autowired
    private DtoMapper mapper;


    @Test
    public void occurrenceToFactorGraphReturnsNullForNullArgument() {
        assertNull(mapper.occurrenceToFactorGraph(null));
    }

    @Test
    public void canMapReturnsTrueForMappableClasses() {
        assertTrue(mapper.canMap(Occurrence.class));
        assertTrue(mapper.canMap(OccurrenceReport.class));
        assertTrue(mapper.canMap(OccurrenceReportDto.class));
    }

    @Test
    public void canMapReturnsFalseForNonMappableClasses() {
        assertFalse(mapper.canMap(String.class));
        assertFalse(mapper.canMap(FactorGraphEdge.class));
    }
}