/*
 * Copyright (C) 2016 Czech Technical University in Prague
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
'use strict';

var React = require('react');
var Reflux = require('reflux');

var Actions = require('../../actions/Actions');
var Logger = require('../../utils/Logger');
var ReportFactory = require('../../model/ReportFactory');
var Report = require('./Report');
var OptionsStore = require('../../stores/OptionsStore'); // Force store initialization, so that it can listen to actions
var TypeaheadStore = require('../../stores/TypeaheadStore');
var ReportStore = require('../../stores/ReportStore');
var RouterStore = require('../../stores/RouterStore');
var Routes = require('../../utils/Routes');

var ReportController = React.createClass({
    mixins: [Reflux.listenTo(ReportStore, 'onReportStoreTrigger')],

    getInitialState: function () {
        return {
            id: null,
            report: this._isNew() ? this.initNewReport() : null,
            revisions: null,
            loading: false
        }
    },

    _isNew: function () {
        return !this.props.params.reportId;
    },

    initNewReport: function () {
        var payload = RouterStore.getTransitionPayload(Routes.createReport.name),
            report = ReportFactory.createOccurrenceReport();
        if (payload) {
            report.initialReports = payload.initialReports;
        }
        return report;
    },

    componentWillMount: function () {
        if (!this._isNew()) {
            this._loadReport(this.props.params.reportId);
        }
    },

    componentDidMount: function () {
        Actions.loadOptions();
        Actions.loadOccurrenceCategories();
        Actions.loadOptions('factorType');
    },

    _loadReport: function (reportId) {
        Actions.loadReport(reportId);
        this.setState({loading: true, id: reportId});
    },

    componentWillReceiveProps: function (nextProps) {
        if (nextProps.params.reportId && this.state.id !== nextProps.params.reportId) {
            this._loadReport(nextProps.params.reportId);
        }
    },

    onReportStoreTrigger: function (data) {
        if (data.action === Actions.loadReport) {
            this._onReportLoaded(data.report);
        } else if (data.action == Actions.loadRevisions) {
            this.setState({revisions: data.revisions});
        }
    },

    _onReportLoaded: function (report) {
        if (!report) {
            this.setState({loading: false});
        } else {
            Logger.log('Loaded report ' + report.id);
            this.setState({report: report, loading: false});
            Actions.loadRevisions(report.fileNumber);
        }
    },


    // Rendering

    render: function () {
        return (<Report report={this.state.report} revisions={this.state.revisions} loading={this.state.loading}/>);
    }
});

module.exports = ReportController;
