package cz.cvut.kbss.reporting.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "javaClass")
@Entity
// Table per class causes the join table between corrective measure request and agent to contain varchar column for agent id
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "AGENT_TYPE")
public abstract class Agent extends AbstractEntity {
}
