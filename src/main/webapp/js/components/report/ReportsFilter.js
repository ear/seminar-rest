/*
 * Copyright (C) 2016 Czech Technical University in Prague
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @jsx
 */
'use strict';

var React = require('react');
var Reflux = require('reflux');
var Button = require('react-bootstrap').Button;

var Constants = require('../../constants/Constants');
var injectIntl = require('../../utils/injectIntl');
var I18nMixin = require('../../i18n/I18nMixin');
var JsonLdUtils = require('../../utils/JsonLdUtils').default;
var OptionsStore = require('../../stores/OptionsStore');
var ReportType = require('../../model/ReportType');
var Select = require('../Select');
var Vocabulary = require('../../constants/Vocabulary');

var ReportsFilter = React.createClass({
    mixins: [I18nMixin, Reflux.listenTo(OptionsStore, '_onPhasesLoaded')],

    propTypes: {
        onFilterChange: React.PropTypes.func.isRequired,
        reports: React.PropTypes.array,
        filter: React.PropTypes.object
    },

    getInitialState: function () {
        var filterInit = this.props.filter ? this.props.filter : {};
        return {
            'phase': filterInit['phase'] ? filterInit['phase'] : Constants.FILTER_DEFAULT,
            'javaClass': filterInit['javaClass'] ? filterInit['javaClass'] : Constants.FILTER_DEFAULT
        }
    },

    _onPhasesLoaded(type) {
        if (type === 'reportingPhase') {
            this.forceUpdate();
        }
    },

    onSelect: function (e) {
        var value = e.target.value;
        var change = {};
        change[e.target.name] = value;
        this.setState(change);
        this.props.onFilterChange(change);
    },

    onResetFilters: function () {
        var newState = {};
        Object.getOwnPropertyNames(this.state).forEach((key) => {
            newState[key] = Constants.FILTER_DEFAULT;
        });
        this.setState(newState);
        this.props.onFilterChange(newState);
    },


    render: function () {
        return <tr className='filter'>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>{this._renderSelect('javaClass', this.state.javaClass, this._getReportTypeOptions())}</td>
            <td>{this._renderSelect('phase', this.state.phase, this._getReportingPhaseOptions())}</td>
            <td >
                <Button bsStyle='primary' bsSize='small' className='reset-filters'
                        onClick={this.onResetFilters}>{this.i18n('reports.filter.reset')}</Button>
            </td>
        </tr>;
    },

    _renderSelect: function (name, value, options) {
        options.unshift(this._allFilterOption());
        return <Select name={name} value={value} options={options} onChange={this.onSelect}/>;
    },

    _getReportTypeOptions: function () {
        var types = [],
            options = [],
            reportJavaClass, typeLabel,
            reports = this.props.reports;
        for (var i = 0, len = reports.length; i < len; i++) {
            reportJavaClass = reports[i].javaClass;
            typeLabel = ReportType.getTypeLabel(reportJavaClass);
            if (typeLabel && types.indexOf(reportJavaClass) === -1) {
                types.push(reportJavaClass);
                options.push({value: reportJavaClass, label: this.i18n(typeLabel)});
            }
        }
        return options;
    },

    _allFilterOption: function () {
        return {value: Constants.FILTER_DEFAULT, label: this.i18n('reports.filter.type.all')};
    },

    _getReportingPhaseOptions: function () {
        var phases = OptionsStore.getOptions('reportingPhase'),
            options = [];
        for (var i = 0, len = phases.length; i < len; i++) {
            options.push({
                value: phases[i]['@id'],
                label: JsonLdUtils.getLocalized(phases[i][Vocabulary.RDFS_LABEL], this.props.intl)
            });
        }
        return options;
    }
});

module.exports = injectIntl(ReportsFilter);
